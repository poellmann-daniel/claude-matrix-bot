import anthropic
import uuid
import time

class Message:
    def __init__(self, role, message):
        self.role = role
        self.message = message


class Chat:

    def __init__(self, api_key, event_id=None, room_id=None, options=None):
        if event_id is None:
            self.event_id = str(uuid.uuid4())
        else:
            self.event_id = event_id
        if room_id is None:
            self.room_id = str(uuid.uuid4()) # TODO: remove, this does not make sense anymore
        else:
            self.room_id = room_id
        if options is None:
            self.options = {}
        else:
            self.options = options

        self.retry_counter = 0
        self.max_retry = 20
        self.retry_interval = 15
        self.history = []
        self.client = anthropic.Anthropic(api_key=api_key)

    async def send_temp_message(self, bot, room_id, event_id, message):
        content = {
            "body": f"{message}",
            "msgtype": "m.text",
            "m.relates_to": {
                "rel_type": "m.thread",
                "event_id": event_id
            }
        }
        await bot.async_client.room_send(room_id, message_type="m.room.message", content=content)

    async def say(self, message, bot=None, options=None):
        if options is None:
            options = {}

        self.history.append(Message("user", message))
        overloaded = True
        while overloaded:
            try:

                message = self.client.messages.create(
                    model=self.options['model'] if 'model' in self.options else "claude-3-opus-20240229",
                    max_tokens=self.options['max_tokens'] if 'max_tokens' in self.options else 1000,
                    temperature=self.options['temperature'] if 'temperature' in self.options else 0.0,
                    system=self.options['system_prompt'] if 'system_prompt' in self.options else "",
                    messages=list(map(lambda m: {'role': m.role, 'content': m.message}, self.history))
                )
                response = ""
                content_blocks = message.content
                for block in content_blocks:
                    response += block.text

                self.history.append(Message("assistant", response))
            except Exception as e:
                if self.retry_counter >= self.max_retry:
                    response = f"There was an issue with calling the API.\n(Error: {e}). Will no longer retry."
                    break
                else:
                    reply = f"The API gave the following error:\n{e}\nI will retry ({self.retry_counter}/{self.max_retry})."
                    await self.send_temp_message(bot=bot, room_id=options['room_id'], event_id=options['event_id'], message=reply)
                    time.sleep(self.retry_interval)
                    continue


            overloaded = False
            self.retry_counter = 0

        return response
